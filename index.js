let user = {
	name: 'Paolo Sanhi',
	age: 25,
	pokemon: ['Squirtle', 'Charmander', 'Bulbasaur'],
	friends: {
		hoenn: 'Brock',
		kanto: 'Misty'
	},
	talk: function(){
		console.log('Squirtle! I choose you')
	}
}
console.log(user)

console.log(typeof friends)
console.log(typeof pokemon)
console.log(typeof talk)
console.log('The result of dot notation')
console.log(user.name)
console.log('The result of bracket notation')
console.log(user.pokemon)
console.log('The result of talk method')
console.log('Squirtle! I choose you')




 function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health = target.health - this.attack;
        console.log(target.name + "'s health is now reduced to " +  target.health);

        	if(target.health<=0){
        		target.faint()
        	}
        	console.log(target)
        };
    this.faint = function(){
        console.log(this.name + 'fainted.');
    }

}

// Creates new instances of the "Pokemon" object each with their unique properties
let squirtle = new Pokemon('Squirtle', 30);
let blastoise = new Pokemon('Blastoise', 60);
let treecko = new Pokemon('Treecko', 120);
console.log(squirtle)
console.log(blastoise)
console.log(treecko)

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
squirtle.tackle(blastoise);